#!/usr/bin/env python3
import cgi
import codecs
from bs4 import BeautifulSoup
import random

def parse(path):
    page = open(path, 'r')
    soup = BeautifulSoup(page.read(), 'html.parser')
    page.close()
    questions = []
    answers = []
    tabels = soup.find_all('table')
    for table in tabels:
        q_and_a = table.find_all('td')
        questions += [x.text for x in q_and_a[::2]]
        answers += [x.text for x in q_and_a[1::2]]
    return questions, answers


def get_random(questions, answers):
    random.seed(random.randint(-1000, 1000))
    index = random.randint(0, min(len(questions), len(answers)))
    return questions[index], answers[index]


questions, answers = parse('./data/2019.html')

q, a = get_random(questions, answers)
a = a if len(a) > 0 else "Нет ответа"
def change_on_button():
    global q, a
    q, a = get_random(questions, answers)
    return " "
print("Content-type: text/html")
print()
print("<meta charset=utf-8>")
print("""
<head>
<style>
.exp{visibility: hidden;}
</style>
</head>
""")
print("""
<body>
<script>
function unhide() {
    var hid = document.getElementsByClassName("exp");
    // Emulates jQuery $(element).is(':hidden');
    if(hid[0].offsetWidth > 0 && hid[0].offsetHeight > 0) {
        hid[0].style.visibility = "visible";
    }
}
function next() {
    location.reload();
}
</script>
""")
print('<div class="q">Question:', q, "</div>") 
print("""
<button onclick="unhide()">Show Answer</button>
<div class="exp">
    <p class="and">{}</p>
</div>
""".format(a))
print("""
<button onclick="next()">Next question</button>
""")
print("</body>")
